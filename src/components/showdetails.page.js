import React, {Component} from 'react';
import ShowDetailsCss from '../components/showdetails.page.css';
import { Link } from 'react-router-dom';
import GoBack from'../images/Icon ionic-ios-arrow-back@2x.png';
export default class ShowDetails extends Component {
    constructor(props) {
      super(props);
     
    }
 

 render(){
   return(
 
    <div className="view-page">
    <div className="view-page-content">
        <div className="title"><h2>view posts</h2></div>
        <div class="go-back">
          <Link 
          to="/read">
            <div className="go-back-logo">
          <img src={GoBack} className="goback-img"></img>
          </div>
          </Link>
    <div className="goback-content">
      <Link
      className="goback-content-link"
      to="/read"
      >crud posts</Link>
      <Link 
      to="/create"
      className=""
      > > view posts </Link>
       
    </div>
        </div>
    
    
    
    
    
    
    
    
            <div class="data-box">
              
            <h4>{this.props.match.params.title}</h4>
            <div>ID:{this.props.match.params.id}</div>
            <p> {this.props.match.params.body}</p>

            </div>
    </div>
             </div>
   )
 }
}