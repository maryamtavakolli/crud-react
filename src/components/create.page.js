import React,{Component} from 'react';
import "./create.page.css";
import GoBack from'../images/Icon ionic-ios-arrow-back@2x.png';
import { Link } from 'react-router-dom';


export default class Create extends Component {

constructor(props){
    super(props)
    this.state={
        title:"",
        body:"",
        userId:"",
        flag:true,
        
    }
}



createResource=()=>{
        const {title,body,userId} = this.state;
 




        fetch('https://jsonplaceholder.typicode.com/posts', {
    method: 'POST',
    body: JSON.stringify({
      title: title,
      body: body,
      userId: userId
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })
  .then(response => response.json())
  .then(json => {
      console.log(`json:`);
    console.log(json.title);
  
    console.log(json);

    this.setState({
      title:json.title,
      body:json.body,
      userId:json.userId,
      id:json.id,
      
    })
      }  )



      this.setState({
               title: '',
               body: '',
               userId: "",
             });  

             this.props.history.push("/read/:title"); 
 
    }

 


      changeHandlerTitle = (event) => {
        //   console.log(event);
        const {name,value} = event.target;
        // console.log(value);
        
        this.setState({
            title:value,
          
          });
      }


      changeHandlerBody = (event) => {
        // console.log(event);
      const {name,value} = event.target;
    //   console.log(value);
      
      this.setState({
          body:value,
        
        });
    // console.log(this.state);
    }




    changeHandlerUserId = (event) => {
        // console.log(`event:${event}`);
      const {name,value} = event.target;
    //   console.log(value);
      
      this.setState({
          userId:event.target.value,
        
        });
    // console.log(this.state);
    }






    render(){
        return(
 
         <div className="create-page">
<div className="create-page-content">
    <div className="title"><h2>Create posts</h2></div>
    <div class="go-back">
      <Link 
      to="/read">
        <div className="go-back-logo">
      <img src={GoBack} className="goback-img"></img>
      </div>
      </Link>
<div className="goback-content">
  <Link
  className="goback-content-link"
  to="/read"
  >crud posts</Link>
  <Link 
  to="/create"
  className=""
  > > Create posts </Link>
   
</div>
    </div>








        <div class="data-box">
<label>
<input type="text" placeholder="Title" value={this.state.title} autoComplete="off"  onChange={this.changeHandlerTitle}></input>
</label>
<textarea  className="textarea" type="text" placeholder="body" value={this.state.body} onChange={this.changeHandlerBody}></textarea>
<div  className="buttons">
<Link 
           onClick={this.createResource}
         
           title='Add User'
           to={`/read/${this.state.title}/${this.state.body}/${this.state.flag}`}

           >
       <button className="publish">publish</button>
           
           </Link>


           <Link to={`/read`} >
           <button className="cancel"> cancel</button>
            </Link>



</div>


        </div>
</div>
         </div>

        )
    }}