import React, {Component} from 'react';
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom';
import "./edit.page.css";
import GoBack from'../images/Icon ionic-ios-arrow-back@2x.png';
export default class Edit extends Component {
    constructor(props) {
      super(props);
      this.state={
      title:this.props.match.params.title,
      id:props.match.params.id,
      body:this.props.match.params.body,
      userId:this.props.match.params.userId,
     
    }
  }
 
  componentDidMount(){//miad data morede nazar ro az database mikhone

    fetch(`/posts/${this.state.id}`, {
    method: 'PUT',
    body: JSON.stringify({
      id:this.state.id,
      title:this.state.title,
      body: this.state.body,
      userId: this.state.userId,
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })
  .then(response => response.json())
  .then(json => console.log(json))
  }

    onSubmit(e) {

      fetch('https://jsonplaceholder.typicode.com/posts/1', {
    method: 'PATCH',
    body: JSON.stringify({
      title: this.state.title,
      body:this.state.body,
      id:this.state.id,
      userId:this.state.userId,
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    }
  })
  .then(response => response.json())
  .then(json =>{
    
    console.log(json);
  this.setState({
    title:json.title,
    body:json.body,
    userId:json.userId,
    id:json.id,
  });
    }
  
  )

        e.preventDefault();
        this.props.history.push(`/read/${this.state.title}/${this.state.id}`);
      }
   
    

      onChangeResourseTitle(e) {
        this.setState({ title: e.target.value });
      }


      onChangeResourseId(e) {
        this.setState({ Id: e.target.value });
      }
      onChangeResourseBody(e) {
        this.setState({ Body: e.target.value });
      }


    render() {
        
      return (
        
        <div className="edit-page">
        <div className="edit-page-content">
            <div className="title"><h2>Edit posts</h2></div>
            <div class="go-back">
              <Link 
              to="/read">
                <div className="go-back-logo">
              <img src={GoBack} className="goback-img"></img>
              </div>
              </Link>
        <div className="goback-content">
          <Link
          className="goback-content-link"
          to="/read"
          >crud posts</Link>
          <Link 
          to="/Edit"
          className=""
          > > Edit posts </Link>
           
        </div>
            </div>
        
        
        
        
        
        
        
        
            <form   onSubmit={this.onSubmit}>
                <div class="data-box">
                  
                  <input placeholder={this.props.match.params.title}  onChange={this.onChangeResourseTitle.bind(this)} type="text"></input>
                  <textarea placeholder={this.props.match.params.body} onChange={this.onChangeResourseBody.bind(this)}></textarea>
                  <div className="buttons"> 
                  <Link 
                  to="/read"
                  className="cancel">cancel</Link>
                  <button className="accept" onClick={this.onSubmit.bind(this)}>Accept</button>
                  </div>

                </div>
     
</form>

</div>


                 </div>
        

         
      )
    }
  }