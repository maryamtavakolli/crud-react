import React, {Component} from 'react';
import { Route, Link, BrowserRouter as Router, Switch } from 'react-router-dom';
import "./read.page.css";
import CreateLogo from"../images/Icon feather-file-plus@3x.png";
export default class Read extends Component {
    constructor(props) {
      super(props);
      this.state = {
        listresource:[],
       title: '',
        body: '',
        userId: '',
       
      
      }
    }
    componentDidMount(){

      const {listresource,title,body,userId} = this.state;
     
      fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then((json) => {
      // console.log(`json:${json}`);

  
    this.setState({

      listresource:json,
   
    })




    })
  }



  
    submitHandler = () => {
      const {listresource,title,body,userId} = this.state;
      this.setState({
        listresource: [...listresource, { title:title, body:body, userId:userId}],
        title: '',
        body: '',
       userId: '',
     
      
      });

  
    }

  
  

    delUser = userDel => {
      fetch(`https://jsonplaceholder.typicode.com/posts/${userDel.id}`, {
  method: 'DELETE'
})


      const userWithout = this.state.listresource.filter(user => user.id !==userDel.id)
      this.setState({listresource : userWithout  })
    };
    











    render() {
      console.log(this.props.match.params.flag);
      return (
<div className="read-page">
  <div className="read-page-content">
            <div className="title"><h1>Crud posts</h1></div>
            <Link
                    className="read-page-create-new-post"
                    to={`/create`}
                  >
                       
            
            <img  className="read-page-create-new-post-logo"src={CreateLogo}></img>
            <p>Create Post</p>
        
                  </Link>
         
            {this.props.match.params.flag=="true" ?        <div className="read-page-posts"> 



<div className="read-page-posts-content">
<div className="post-number">Create Post</div>
                 <h3 className="post-title">
                  <p> title:{this.props.match.params.title}</p>
                  </h3>
                  <div className="post-body"> <p> body:{this.props.match.params.body}</p> </div>
                 
                  <div className="post-edit-view-delete">
                   <div className="div-post-edit">
                
                   <Link
                    className="post-edit"
                    to={`/edit/${this.props.match.params.id}/${this.props.match.params.title}/${this.props.match.params.body}/${this.props.match.params.userId}`}
                
                  >
                    Edit
                  </Link>
                  </div>
                  <div className="div-post-delete">
                   <Link 
                   className="post-delete"
                   to={`/read`}
                   onClick={this.delUser.bind(this, this.props.match.params)}
                  >delete
                  </Link>
                   </div>

                   <div className="div-post-view"> 
                   <Link
                   className="post-view"
                    to={`/Showdetails/${this.props.match.params.id}/${this.props.match.params.title}/${this.props.match.params.body}`}
                  >
                    View
                  </Link>
                   </div>
                   </div>
                    </div>
                    </div> :""}



            {/* ........... */}
            


{/* .................................... */}

            <div className="read-page-posts"> 



                <div className="read-page-posts-content">
                {this.state.listresource.map(user =>
             <div key={user.id}>
                  <div className="post">
               
                 <div className="post-number">Post {user.id}</div>
                 <h3 className="post-title">
                    {
                      user.id == this.props.match.params.id ? this.props.match.params.title : user.title
                        }
                  </h3>
                 <div className="post-body"> {user.body}</div>
                 <div className="post-edit-view-delete">
                   <div className="div-post-edit">
                
                   <Link
                    className="post-edit"
                    to={`/edit/${this.props.match.params.id}/${this.props.match.params.title}/${this.props.match.params.body}/${this.props.match.params.userId}`}
                
                  >
                    Edit
                  </Link>
                  </div>
                   <div className="div-post-delete">
                   <Link 
                   className="post-delete"
                   to={`/read`}
                   onClick={this.delUser.bind(this, user)}
                  >delete
                  </Link>
                   </div>
                   <div className="div-post-view"> 
                   <Link
                   className="post-view"
                    to={`/ShowDetails/${user.id }/${user.title}/${user.body}`}
                  >
                    View
                  </Link>
                   </div>
                   </div>
                    </div>
                 
                 
                 </div>
                  )}
                </div>
                  
                </div>
             </div>
</div>

      )
    }}