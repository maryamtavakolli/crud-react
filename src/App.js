import React, {Component} from 'react';
import './App.css';
import Read from "./components/read.page";
import Create from './components/create.page';
import Home from "./components/home.page";
import  ShowDetails from "./components/showdetails.page";
import { Router, Switch, Route } from "react-router-dom";
import history from './components/history.page';
import Edit from "./components/edit.page";


function App(){

  return(

    <div>




     <Router history={history}>
                <Switch>
                  
              <Route exact path="/read/:title/:body/:flag" render={(props) => <Read {...props}  />} />
              />
                    <Route path="/" exact component={Home} />
                    <Route path="/create" component={Create} />
     
                    <Route path="/read" component={Read} />
                 
                    <Route path="/Edit/:id/:title/:body" component={Edit} />
                    <Route path="/showdetails/:id/:title/:body" component={ShowDetails} />
                  
                </Switch>
            </Router>



    
    </div>
  )
}
export default App;